# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bitstream',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=500)),
                ('handle', models.CharField(max_length=500)),
                ('type', models.CharField(max_length=500)),
                ('expand', models.CharField(max_length=500)),
                ('bundleName', models.CharField(max_length=500)),
                ('description', models.TextField(default=b'')),
                ('format', models.CharField(max_length=500)),
                ('mimeType', models.CharField(max_length=500)),
                ('retrieveLink', models.CharField(max_length=500)),
                ('checkSum', models.CharField(max_length=500)),
                ('checkSumAlgorithm', models.CharField(max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='Collection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=500)),
                ('handle', models.CharField(max_length=500)),
                ('type', models.CharField(max_length=500)),
                ('expand', models.CharField(max_length=500)),
                ('logo', models.CharField(max_length=500, null=True)),
                ('license', models.CharField(max_length=500, null=True)),
                ('copyrightText', models.CharField(default=b'', max_length=500)),
                ('introductoryText', models.CharField(default=b'', max_length=500)),
                ('shortDescription', models.TextField(default=b'', max_length=500)),
                ('sidebarText', models.CharField(default=b'', max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='Community',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=500)),
                ('handle', models.CharField(max_length=500)),
                ('type', models.CharField(max_length=500)),
                ('link', models.CharField(max_length=500)),
                ('expand', models.CharField(max_length=500)),
                ('logo', models.CharField(max_length=500, null=True)),
                ('copyrightText', models.CharField(default=b'', max_length=500)),
                ('introductoryText', models.CharField(default=b'', max_length=500)),
                ('shortDescription', models.TextField(default=b'', max_length=500)),
                ('sidebarText', models.CharField(default=b'', max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=500)),
                ('handle', models.CharField(max_length=500)),
                ('type', models.CharField(max_length=500)),
                ('link', models.CharField(max_length=500)),
                ('expand', models.CharField(max_length=500)),
                ('archived', models.CharField(max_length=500)),
                ('withdrawn', models.CharField(max_length=500)),
                ('keyword', models.CharField(max_length=500)),
                ('parentCollection', models.ForeignKey(related_name='collection_fk', to='service.Collection', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='MetadataEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(max_length=500)),
                ('value', models.CharField(max_length=500)),
                ('language', models.CharField(max_length=500)),
                ('parentObject', models.ForeignKey(to='service.Item')),
            ],
        ),
        migrations.CreateModel(
            name='ResourcePolicy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('action', models.CharField(max_length=500)),
                ('resourceType', models.CharField(max_length=500)),
                ('rpDescription', models.CharField(max_length=500, null=True)),
                ('rpName', models.CharField(max_length=500, null=True)),
                ('rpType', models.CharField(max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fullname', models.CharField(max_length=500)),
                ('token', models.CharField(max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.CharField(max_length=500)),
                ('password', models.CharField(max_length=500)),
            ],
        ),
        migrations.AddField(
            model_name='collection',
            name='parentCommunity',
            field=models.ForeignKey(related_name='community_fk', to='service.Community', null=True),
        ),
        migrations.AddField(
            model_name='bitstream',
            name='parentObject',
            field=models.ForeignKey(to='service.Item'),
        ),
        migrations.AddField(
            model_name='bitstream',
            name='policies',
            field=models.ForeignKey(to='service.ResourcePolicy'),
        ),
    ]
