from django.db import models
from django import forms
from django.forms.widgets import TextInput, Textarea, Select, DateTimeInput
import datetime


# Listas de seleccion de las pantallas
# Opciones de Nivel de recurso educativo
NivelChoices = (
    (1, 'Muy Alto'),
    (2, 'Alto'),
    (3, 'Medio'),
    (4, 'Bajo'),
)
# Opciones de extensiones de recurso educativo
ExtensionChoices = (
    (1, 'Pdf'),
    (2, 'doc'),
    (3, 'xlsx'),
    (4, 'mp4'),
    (5, 'ppt'),
    (6, 'jpeg'),
    (7, 'gif'),
    (8, 'png'),
)
# Opciones de paises de recurso educativo
PaisChoices = (
    (1, 'Colombia'),
    (2, 'Argentina'),
    (3, 'Brasil'),
    (4, 'Bolivia'),
    (5, 'Ecuador'),
    (6, 'Peru'),
)
# Opciones de idiomas de recurso educativo
LenguajeChoices = (
    (1, 'Espanol'),
    (2, 'Ingles'),
    (3, 'Frances'),
    (4, 'Portugues'),
)


# Modelo de entidades correspondiente a modelo de rest-api de DSPACE
# https://wiki.duraspace.org/display/DSDOC5x/REST+API#RESTAPI-Model-Objectdatatypes

# Class Community:
# Communities in DSpace are used for organization and hierarchy, and are containers that hold
# sub-Communities and Collections. (ex: Department of Engineering)
class Community(models.Model):
    name = models.CharField(max_length=500)
    handle = models.CharField(max_length=500)
    type = models.CharField(max_length=500)
    link = models.CharField(max_length=500)
    expand = models.CharField(max_length=500)
    logo = models.CharField(null=True, max_length=500)
    copyrightText = models.CharField(default="", max_length=500)
    introductoryText = models.CharField(default="", max_length=500)
    shortDescription = models.TextField(default="", max_length=500)
    sidebarText = models.CharField(default="", max_length=500)
    countItems = models.IntegerField


# Class Collection
# Collections in DSpace are containers of Items. (ex: Engineering Faculty Publications)
class Collection(models.Model):
    name = models.CharField(max_length=500)
    handle = models.CharField(max_length=500)
    type = models.CharField(max_length=500)
    link = models.URLField
    expand = models.CharField(max_length=500)
    logo = models.CharField(null=True, max_length=500)
    parentCommunity = models.ForeignKey(Community, related_name="community_fk", null=True)
    license = models.CharField(null=True, max_length=500)
    copyrightText = models.CharField(default="", max_length=500)
    introductoryText = models.CharField(default="", max_length=500)
    shortDescription = models.TextField(default="", max_length=500)
    sidebarText = models.CharField(default="", max_length=500)
    numberItems = countItems = models.IntegerField


# Class Item
# Items in DSpace represent a "work" and combine metadata and files, known as Bitstreams.
class Item(models.Model):
    name = models.CharField(max_length=500)
    handle = models.CharField(max_length=500)
    type = models.CharField(max_length=500)
    link = models.CharField(max_length=500)
    expand = models.CharField(max_length=500)
    parentCollection = models.ForeignKey(Collection, related_name="collection_fk", null=True)
    archived = models.CharField(max_length=500)
    withdrawn = models.CharField(max_length=500)
    keyword = models.CharField(max_length=500)
    autor = models.CharField(max_length=500)
    pais = models.CharField(max_length=500)
    descripcion = models.CharField(max_length=500)
    fecha = models.CharField(max_length=500)


class ResourcePolicy(models.Model):
    action = models.CharField(max_length=500)
    epersonId = models.IntegerField
    groupId = models.IntegerField
    resourceId = models.IntegerField
    resourceType = models.CharField(max_length=500)
    rpDescription = models.CharField(null=True, max_length=500)
    rpName = models.CharField(null=True, max_length=500)
    rpType = models.CharField(max_length=500)
    startDate = models.DateField
    endDate = models.DateField


# Class Bitstream
# Bitstreams are files. They have a filename, size (in bytes), and a file format. Typically in DSpace,
# the Bitstream will the "full text" article, or some other media. Some files are the actual file that
#  was uploaded (tagged with bundleName:ORIGINAL), others are DSpace-generated files that are derivatives or renditions
# , such as text-extraction, or thumbnails. You can download files/bitstreams.
# DSpace doesn't really limit the type of files that it takes in, so this could be PDF, JPG, audio, video, zip, or other.
#  Also, the logo for a Collection or a Community, is also a Bitstream.
class Bitstream(models.Model):
    name = models.CharField(max_length=500)
    handle = models.CharField(max_length=500)
    type = models.CharField(max_length=500)
    link = models.URLField
    expand = models.CharField(max_length=500)
    bundleName = models.CharField(max_length=500)
    description = models.TextField(default="")
    format = models.CharField(max_length=500)
    mimeType = models.CharField(max_length=500)
    sizeBytes = models.BigIntegerField
    parentObject = models.ForeignKey(Item)
    retrieveLink = models.CharField(max_length=500)
    checkSum = models.CharField(max_length=500)
    checkSumAlgorithm = models.CharField(max_length=500)
    sequenceId = models.IntegerField
    policies = models.ForeignKey(ResourcePolicy)


class MetadataEntry(models.Model):
    key = models.CharField(max_length=500)
    value = models.CharField(max_length=500)
    language = models.CharField(max_length=500)
    parentObject = models.ForeignKey(Item)


class User(models.Model):
    email = models.CharField(max_length=500)
    password = models.CharField(max_length=500)


class Status(models.Model):
    okay = models.BooleanField
    authenticated = models.BooleanField
    email = models.EmailField
    fullname = models.CharField(max_length=500)
    token = models.CharField(max_length=500)


# Creacion de Formularios customizados

# Formulario de recusro educativo
class RecursoForm(forms.Form):
    titulo = forms.CharField(max_length=2048, required=True, widget=TextInput(attrs={'size': 67}))
    descripcion = forms.CharField(max_length=2048, required=True, widget=Textarea(attrs={'rows': 4, 'cols': 66}))
    autor = forms.CharField(max_length=2048, required=True, widget=TextInput(attrs={'readonly': True}),
                            initial="Profesor1")
    nivelAgregacion = forms.ChoiceField(choices=NivelChoices, required=True)
    extension = forms.ChoiceField(choices=ExtensionChoices, required=True, widget=Select)
    lenguaje = forms.ChoiceField(choices=LenguajeChoices, required=True)
    pais = forms.ChoiceField(choices=PaisChoices, required=True)
    palabrasClaves = forms.CharField(max_length=2048, required=True, widget=TextInput)
    fecha = forms.DateTimeField(required=True, widget=DateTimeInput(attrs={'class': 'datepicker', 'readonly': True}),
                                initial=datetime.datetime.now())
