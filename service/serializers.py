__author__ = 'root'

from service.models import Item, MetadataEntry
from rest_framework import serializers


# Serializar modelo de Item
class ItemSerialializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = (
            "id",
            "name",
            "handle",
            "link",
            "expand",
            "parentCollection",
            "archived",
            "withdrawn",
            "keyword",
            "autor",
            "pais",
            "descripcion",
            "fecha",
        )


# Serializar modelo de Metadata
class MetadataSerializer(serializers.ModelSerializer):
    class Meta:
        model = MetadataEntry
        fields = (
            "key",
            "value",
            "language",
            "parentObject",
        )
