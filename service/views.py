__author__ = 'jc.cerquera10@uniandes.edu.co'
from service.models import Item
from service.serializers import ItemSerialializer
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view

''' Servicio rest que retorna todos los items '''


@api_view(['GET'])
def itemlist(request, format=None):
    items = Item.objects.all()
    serializer = ItemSerialializer(items, many=True)
    return Response(serializer.data)


''' Servicio rest que retorna un item por id '''


@api_view(['GET'])
def itemdetail(request, pk):
    try:
        item = Item.objects.get(pk=pk)
    except Item.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    serializer = ItemSerialializer(item)
    return Response(serializer.data)


''' Servicio rest de consulta de items por palabra clave'''


@api_view(['GET'])
def itemkeyword(request, keyword):
    items = Item.objects.filter(keyword__icontains=keyword)
    serializer = ItemSerialializer(items, many=True)
    return Response(serializer.data)
