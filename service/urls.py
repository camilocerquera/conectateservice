__author__ = 'jc.cerquera10@uniandes.edu.co'
from service.views import itemdetail, itemlist, itemkeyword
from django.conf.urls import include, url
from django.contrib import admin
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^items/$', itemlist),
    url(r'^items/(?P<pk>[0-9]+)/$', itemdetail),
    url(r'^items/(?P<keyword>\w+)/keyword/$', itemkeyword),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]

urlpatterns = format_suffix_patterns(urlpatterns)

